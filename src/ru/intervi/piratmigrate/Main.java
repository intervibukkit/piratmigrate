package ru.intervi.piratmigrate;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;

public class Main extends JavaPlugin implements Listener {
	public String WORLDPATH;
	public final boolean ONLINEMODE = Bukkit.getOnlineMode();
	public volatile boolean start = false;
	
	public String getPluginsPath() {
		return new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile().getAbsolutePath();
	}
	
	public static String getHTTPSpage(String addr) {
		try {
			String result = "";
			URL url = new URL(addr);
			HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
			BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
			while(reader.ready()) result += reader.readLine() + '\n';
			reader.close();
			return result.trim();
		} catch(Exception e) {e.printStackTrace(); return null;}
	}
	
	public UUID getPiratUUID(String name) {
		return UUID.nameUUIDFromBytes(("OfflinePlayer:" + name).getBytes());
	}
	
	private class Data {
		public UUID id;
	}
	
	public UUID getLicenseUUID(String name) {
		String json = getHTTPSpage("https://api.mojang.com/users/profiles/minecraft/" + name);
		if (json == null) return null;
		return new Gson().fromJson(json, Data.class).id;
	}
	
	public UUID getPiratUUID(UUID uuid) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		if (player == null || player.getName() == null || player.getName().trim().isEmpty()) return null;
		return getPiratUUID(player.getName());
	}
	
	public UUID getLicenseUUID(UUID uuid) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		if (player == null) return null;
		return getLicenseUUID(Bukkit.getOfflinePlayer(uuid).getName());
	}
	
	public List<UUID> getPlayers() {
		ArrayList<UUID> result = new ArrayList<UUID>();
		File f = new File(WORLDPATH + File.separatorChar + "playerdata");
		for (String s : f.list()) result.add(UUID.fromString(s.substring(0, s.indexOf('.'))));
		return result;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender.hasPermission("piratmigrate.admin")) {
			sender.sendMessage("migrate will started");
			//for (Player p : Bukkit.getOnlinePlayers()) p.kickPlayer("server will migrating online-mode");
			//Bukkit.setWhitelist(true);
			WORLDPATH = Bukkit.getWorlds().get(0).getWorldFolder().getAbsolutePath();
			new Threads(this, sender).go();
		} else sender.sendMessage("no permission");
		return true;
	}
}