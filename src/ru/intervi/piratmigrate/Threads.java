package ru.intervi.piratmigrate;

import org.bukkit.command.CommandSender;

import ru.intervi.piratmigrate.plugins.*;

public class Threads {
	public Threads(Main main, CommandSender sender) {
		this.main = main;
		this.sender = sender;
	}
	
	private Main main;
	private CommandSender sender;
	
	public void go() {
		new Minecraft(main, sender).start();
		new Essentials(main, sender).start();
		new MonVote(main, sender).start();
		new PerWorldInventory(main, sender).start();
		new PlayersWarp(main, sender).start();
		new WorldGuard(main, sender).start();
	}
}