package ru.intervi.piratmigrate.plugins;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import ru.intervi.piratmigrate.Main;

public class PlayersWarp extends Thread {
	public PlayersWarp(Main main, CommandSender sender) {
		this.main = main;
		this.sender = sender;
	}
	
	private Main main;
	private CommandSender sender;
	
	public void run() {
		try {
			File file = new File(main.getPluginsPath() + File.separatorChar + "PlayersWarp" + File.separatorChar + "database.yml");
			if (!file.isFile()) return;
			FileConfiguration db = YamlConfiguration.loadConfiguration(file);
			ArrayList<String> replace = new ArrayList<String>();
			for (String s : db.getStringList("users")) {
				UUID uuid = UUID.fromString(s.split("|")[0]);
				UUID newuuid = main.ONLINEMODE ? main.getLicenseUUID(uuid) : main.getPiratUUID(uuid);
				if (newuuid == null) continue;
				replace.add(newuuid.toString() + '|' + s.split("|")[1]);
			}
			db.set("users", replace);
			db.save(file);
			sender.sendMessage("PlayersWarp renaming done");
		} catch(Exception e) {e.printStackTrace(); sender.sendMessage("PlayersWarp error");}
	}
}