package ru.intervi.piratmigrate.plugins;

import org.bukkit.command.CommandSender;

import ru.intervi.piratmigrate.Main;

import java.io.File;
import java.util.UUID;

public class PerWorldInventory extends Thread {
	public PerWorldInventory(Main main, CommandSender sender) {
		this.main = main;
		this.sender = sender;
	}
	
	private Main main;
	private CommandSender sender;
	
	public void run() {
		String path = main.getPluginsPath() + File.separatorChar + "PerWorldInventory" + File.separatorChar + "data";
		if (!(new File(path).isDirectory())) return;
		for (String s : new File(path).list()) {
			try {
				UUID uuid = UUID.fromString(s);
				UUID newuuid = main.ONLINEMODE ? main.getLicenseUUID(uuid) : main.getPiratUUID(uuid);
				if (newuuid == null) continue;
				File old = new File(path + File.separatorChar + s);
				File newf = new File(path + File.separatorChar + newuuid.toString());
				if (newf.isFile()) continue;
				old.renameTo(newf);
			} catch(Exception e) {e.printStackTrace(); sender.sendMessage("PerWorldInventory error");}
		}
		sender.sendMessage("PerWorldInventory renaming done");
	}
}