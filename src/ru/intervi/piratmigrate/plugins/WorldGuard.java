package ru.intervi.piratmigrate.plugins;

import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import ru.intervi.piratmigrate.Main;

public class WorldGuard extends Thread {
	public WorldGuard(Main main, CommandSender sender) {
		this.main = main;
		this.sender = sender;
	}
	
	private Main main;
	private CommandSender sender;
	
	private WorldGuardPlugin getWorldGuard() { //получить WorldGuard
	    Plugin plugin = main.getServer().getPluginManager().getPlugin("WorldGuard");
	 
	    // WorldGuard may not be loaded
	    if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
	        return null; // Maybe you want throw an exception instead
	    }
	 
	    return (WorldGuardPlugin) plugin;
	}
	
	public void run() {
		try {
			WorldGuardPlugin wg = getWorldGuard();
			if (wg == null) return;
			for (RegionManager manager : wg.getRegionContainer().getLoaded()) {
				for (ProtectedRegion rg : manager.getRegions().values()) {
					for (UUID uuid : rg.getOwners().getUniqueIds()) {
						UUID newuuid = main.ONLINEMODE ? main.getLicenseUUID(uuid) : main.getPiratUUID(uuid);
						if (newuuid == null) continue;
						rg.getOwners().removePlayer(uuid);
						rg.getOwners().addPlayer(newuuid);
					}
					for (UUID uuid : rg.getMembers().getUniqueIds()) {
						UUID newuuid = main.ONLINEMODE ? main.getLicenseUUID(uuid) : main.getPiratUUID(uuid);
						if (newuuid == null) continue;
						rg.getMembers().removePlayer(uuid);
						rg.getMembers().addPlayer(newuuid);
					}
				}
				manager.saveChanges();
			}
			sender.sendMessage("WorldGuard renaming done");
		} catch(Exception e) {e.printStackTrace(); sender.sendMessage("WorldGuard error");}
	}
}