package ru.intervi.piratmigrate.plugins;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import ru.intervi.piratmigrate.Main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.UUID;

public class Essentials extends Thread {
	public Essentials(Main main, CommandSender sender) {
		this.main = main;
		this.sender = sender;
	}
	
	private Main main;
	private CommandSender sender;
	
	public void run() {
		File file = new File(main.getPluginsPath() + File.separatorChar + "Essentials" + File.separatorChar + "usermap.csv");
		File newfile = new File(main.getPluginsPath() + File.separatorChar + "Essentials" + File.separatorChar + "usermap.csv.new");
		if (!file.isFile()) return;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			BufferedWriter writer = new BufferedWriter(new FileWriter(newfile));
			while(reader.ready()) {
				String str = reader.readLine();
				UUID uuid = main.ONLINEMODE ? main.getLicenseUUID(UUID.fromString(str.split(",")[1])) : main.getPiratUUID(UUID.fromString(str.split(",")[1]));
				if (uuid == null) continue;
				writer.write(str.split(",")[0] + ',' + uuid.toString());
				writer.newLine();
			}
			reader.close();
			writer.close();
			file.delete();
			newfile.renameTo(file);
			sender.sendMessage("Essentials renaming done");
		} catch(Exception e) {e.printStackTrace(); sender.sendMessage("Essentials error");}
		String folderPath = main.getPluginsPath() + File.separatorChar + "Essentials" + File.separatorChar + "userdata";
		for (File ud : new File(folderPath).listFiles()) {
			try {
				if (!ud.isFile()) continue;
				FileConfiguration user = YamlConfiguration.loadConfiguration(ud);
				String newUUID = main.ONLINEMODE ? main.getLicenseUUID(user.getString("lastAccountName")).toString() : main.getPiratUUID((user.getString("lastAccountName"))).toString();
				File newFile = new File(folderPath + File.separatorChar + newUUID + ".yml");
				if (newFile.isFile()) continue;
				ud.renameTo(newFile);
			} catch(Exception e) {e.printStackTrace(); sender.sendMessage("Essentials error");}
		}
	}
}
