package ru.intervi.piratmigrate;

import org.bukkit.command.CommandSender;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.UUID;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Minecraft extends Thread {
	public Minecraft(Main main, CommandSender sender) {
		this.main = main;
		this.sender = sender;
	}
	
	private Main main;
	private CommandSender sender;
	private String PATH = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile().getParentFile().getAbsolutePath();
	
	public class OPSData {
		public UUID uuid;
		public String name;
		public int lavel;
		public boolean bypassesPlayerLimit;
	}
	
	public class WhitelistData {
		public UUID uuid;
		public String name;
	}
	
	public class BanData {
		public UUID uuid;
		public String name;
		public String created;
		public String source;
		public String expires;
		public String reason;
	}
	
	private void renameOPs() {
		try {
			String path = PATH + File.separatorChar + "ops.json";
			ArrayList<OPSData> list = new ArrayList<OPSData>();
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			String json = new String(encoded, Charset.defaultCharset());
			Gson gson = new Gson();  
			for (OPSData d : gson.fromJson(json, OPSData[].class)) {
				d.uuid = main.ONLINEMODE ? main.getLicenseUUID(d.name) : main.getPiratUUID(d.name);
				list.add(d);
			}
			Files.write(Paths.get(path), gson.toJson(list).getBytes(Charset.defaultCharset()));
		} catch(Exception e) {e.printStackTrace();}
	}
	
	private void renameWhitelist() {
		try {
			String path = PATH + File.separatorChar + "whitelist.json";
			ArrayList<WhitelistData> list = new ArrayList<WhitelistData>();
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			String json = new String(encoded, Charset.defaultCharset());
			Gson gson = new Gson();  
			for (WhitelistData d : gson.fromJson(json, WhitelistData[].class)) {
				d.uuid = main.ONLINEMODE ? main.getLicenseUUID(d.name) : main.getPiratUUID(d.name);
				list.add(d);
			}
			Files.write(Paths.get(path), gson.toJson(list).getBytes(Charset.defaultCharset()));
		} catch(Exception e) {e.printStackTrace();}
	}
	
	private void renameBans() {
		try {
			String path = PATH + File.separatorChar + "banned-players.json";
			ArrayList<BanData> list = new ArrayList<BanData>();
			byte[] encoded = Files.readAllBytes(Paths.get(path));
			String json = new String(encoded, Charset.defaultCharset());
			Gson gson = new Gson();  
			for (BanData d : gson.fromJson(json, BanData[].class)) {
				d.uuid = main.ONLINEMODE ? main.getLicenseUUID(d.name) : main.getPiratUUID(d.name);
				list.add(d);
			}
			Files.write(Paths.get(path), gson.toJson(list).getBytes(Charset.defaultCharset()));
		} catch(Exception e) {e.printStackTrace();}
	}
	
	public void run() {
		for (UUID uuid : main.getPlayers()) {
			try {
				UUID newUUID = main.ONLINEMODE ? main.getLicenseUUID(uuid) : main.getPiratUUID(uuid);
				if (newUUID == null) continue;
				File old = new File(main.WORLDPATH + File.separatorChar + "playerdata" + File.separatorChar + uuid.toString() + ".dat");
				File newf = new File(main.WORLDPATH + File.separatorChar + "playerdata" + File.separatorChar + newUUID.toString() + ".dat");
				if (newf.isFile()) continue;
				old.renameTo(newf);
				old = new File(main.WORLDPATH + File.separatorChar + "stats" + File.separatorChar + uuid.toString() + ".json");
				newf = new File(main.WORLDPATH + File.separatorChar + "stats" + File.separatorChar + newUUID.toString() + ".json");
				old.renameTo(newf);
			} catch(Exception e) {e.printStackTrace(); sender.sendMessage("Minecraft error");}
		}
		renameOPs();
		renameWhitelist();
		renameBans();
		sender.sendMessage("Minecraft players rename done");
	}
}