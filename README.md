# PiratMigrate #

Для версии сервера: >= 1.8.3

Плагин для осуществления миграции между пираткой и лицензией. Его задача - переименовать все UUID.


## Использование ##

Смените online-mode на желаемый. Установите плагин.

**/pmig** (*piratmigrate.admin*) - команда, начинающая миграцию


## Поддержка плагинов ##

* Essentials
* PerWorldInventory
* WorldGuard
* MonVote
* PlayersWarp